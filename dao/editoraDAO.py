from dao.DAO import DAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from modelo.editora import Editora


class EditoraDAO(DAO):
    def __init__(self):
        super().__init__('editora.pkl')

    def add(self, editora: Editora, **kwargs):
        if (isinstance(editora.id, str)) and (editora is not None) and isinstance(editora, Editora):
            super().add(editora.id, editora)

    def get(self, key: str):
        if isinstance(key, str):
            return super().get(key)

    def remove(self, key: str):
        if isinstance(key, str):
            try:
                return super().remove(key)
            except KeyError:
                raise RequisicaoMalFeita("editora com o id informado não exite")
