from dao.DAO import DAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from modelo.emprestimo import Emprestimo


class EmprestimoDAO(DAO):
    def __init__(self):
        super().__init__('emprestimo.pkl')

    def add(self, emprestimo: Emprestimo, **kwargs):
        if (isinstance(emprestimo.id, str)) and (emprestimo is not None) and isinstance(emprestimo, Emprestimo):
            super().add(emprestimo.id, emprestimo)

    def get(self, key: str):
        if isinstance(key, str):
            return super().get(key)

    def remove(self, key: str):
        if isinstance(key, str):
            try:
                return super().remove(key)
            except KeyError:
                raise RequisicaoMalFeita("emprestimo com o id informado não exite")
