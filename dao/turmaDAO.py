from dao.DAO import DAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from modelo.turma import Turma


class TurmaDAO(DAO):
    def __init__(self):
        super().__init__('turma.pkl')

    def add(self, turma: Turma, **kwargs):
        if (isinstance(turma.id, str)) and (turma is not None) and isinstance(turma, Turma):
            super().add(turma.id, turma)

    def get(self, key: str):
        if isinstance(key, str):
            return super().get(key)

    def remove(self, key: str):
        if isinstance(key, str):
            try:
                return super().remove(key)
            except KeyError:
                raise RequisicaoMalFeita("turma com o id informado não exite")
