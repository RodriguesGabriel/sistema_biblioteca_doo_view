from dao.DAO import DAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from modelo.livro import Livro


class LivroDAO(DAO):
    def __init__(self):
        super().__init__('livro.pkl')

    def add(self, livro: Livro, **kwargs):
        if (isinstance(livro.registro, str)) and (livro is not None) and isinstance(livro, Livro):
            super().add(livro.registro, livro)

    def get(self, key: str):
        if isinstance(key, str):
            return super().get(key)

    def remove(self, key: str):
        if isinstance(key, str):
            try:
                return super().remove(key)
            except KeyError:
                raise RequisicaoMalFeita("livro com o id informado não exite")
