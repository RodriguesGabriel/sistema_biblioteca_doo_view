from dao.DAO import DAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from modelo.autor import Autor
from modelo.editora import Editora
from modelo.livro import Livro


class AutorDAO(DAO):
    def __init__(self):
        super().__init__('autor.pkl')

    def add(self, autor: Autor, **kwargs):
        if (isinstance(autor.id, str)) and (autor is not None) and isinstance(autor, Autor):
            super().add(autor.id, autor)

    def get(self, key: str):
        if isinstance(key, str):
            return super().get(key)

    def remove(self, key: str):
        if isinstance(key, str):
            try:
                return super().remove(key)
            except KeyError:
                raise RequisicaoMalFeita("autor com o id informado não exite")
