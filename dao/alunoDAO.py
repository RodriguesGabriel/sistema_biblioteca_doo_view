from dao.DAO import DAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from modelo.aluno import Aluno


class AlunoDAO(DAO):
    def __init__(self):
        super().__init__('aluno.pkl')

    def add(self, aluno: Aluno, **kwargs):
        if (isinstance(aluno.matricula, str)) and (aluno is not None) and isinstance(aluno, Aluno):
            super().add(aluno.matricula, aluno)

    def get(self, key: str):
        if isinstance(key, str):
            return super().get(key)

    def remove(self, key: str):
        if isinstance(key, str):
            try:
                return super().remove(key)
            except KeyError:
                raise RequisicaoMalFeita("aluno com a matrícula informada não exite")
