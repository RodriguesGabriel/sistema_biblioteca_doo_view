from datetime import date

from modelo.aluno import Aluno
from modelo.livro import Livro


class Emprestimo:
    def __init__(self, id: str, aluno: Aluno, livro: Livro, data_do_emprestimo: date, data_limite: date):
        self.__id = id
        self.__aluno = aluno
        self.__livro = livro
        self.__data_do_emprestimo = data_do_emprestimo
        self.__data_limite = data_limite
        self.__entregue = False
        self.__avaliacao = None

    @property
    def id(self):
        return self.__id

    @property
    def aluno(self):
        return self.__aluno

    @aluno.setter
    def aluno(self, aluno: Aluno):
        self.__aluno = aluno

    @property
    def livro(self):
        return self.__livro

    @livro.setter
    def livro(self, livro: Livro):
        self.__livro = livro

    @property
    def data_do_emprestimo(self):
        return self.__data_do_emprestimo

    @data_do_emprestimo.setter
    def data_do_emprestimo(self, data_do_emprestimo: date):
        self.__data_do_emprestimo = data_do_emprestimo

    @property
    def data_limite(self):
        return self.__data_limite

    @data_limite.setter
    def data_limite(self, data_limite: date):
        self.__data_limite = data_limite

    @property
    def entregue(self):
        return self.__entregue

    @entregue.setter
    def entregue(self, entregue: date):
        self.__entregue = entregue

    @property
    def avaliacao(self):
        return self.__avaliacao

    @avaliacao.setter
    def avaliacao(self, avaliacao: date):
        self.__avaliacao = avaliacao
