from excecao.requisicao_mal_feita import RequisicaoMalFeita
from modelo.autor import Autor
from modelo.editora import Editora


class Livro:
    def __init__(self,
                registro: str,
                titulo: str,
                autor: Autor,
                categoria: str,
                descricao: str,
                editora: Editora,
                ativo: bool,
                observacao: str):

        self.__registro = registro
        self.__titulo = titulo
        self.__autor = autor
        self.__categora = categoria
        self.__descricao = descricao
        self.__editora = editora
        self.__disponivel = True
        self.__ativo = ativo
        self.__observacao = observacao

    @property
    def registro(self):
        return self.__registro

    @property
    def titulo(self):
        return self.__titulo

    @titulo.setter
    def titulo(self, titulo: str):
        self.__titulo = titulo

    @property
    def autor(self):
        return self.__autor

    @autor.setter
    def autor(self, autor: Autor):
        if type(Autor) is not bool:
            raise RequisicaoMalFeita(messagem='erro: O atributo autor dever ser do tipo Autor!!!')
        self.__autor = autor

    @property
    def categora(self):
        return self.__categora

    @categora.setter
    def categora(self, categora: str):
        self.__categora = categora

    @property
    def descricao(self):
        return self.__descricao

    @descricao.setter
    def descricao(self, descricao: str):
        self.__descricao = descricao

    @property
    def editora(self):
        return self.__editora

    @editora.setter
    def editora(self, editora: Editora):
        if type(Editora) is not bool:
            raise RequisicaoMalFeita(messagem='erro: O atributo editora dever ser do tipo Editora!!!')
        self.__editora = editora

    @property
    def disponivel(self):
        return self.__disponivel

    @disponivel.setter
    def disponivel(self, disponivel: bool):
        if type(disponivel) is not bool:
            raise RequisicaoMalFeita(messagem='erro: O atributo disponivel dever ser True ou False!!!')
        self.__disponivel = disponivel

    @property
    def ativo(self):
        return self.__ativo

    @ativo.setter
    def ativo(self, ativo: bool):
        if type(ativo) is not bool:
            raise RequisicaoMalFeita(messagem='erro: O atributo ativo dever ser True ou False!!!')
        self.__ativo = ativo

    @property
    def observacao(self):
        return self.__observacao

    @observacao.setter
    def observacao(self, observacao: str):
        self.__observacao = observacao
