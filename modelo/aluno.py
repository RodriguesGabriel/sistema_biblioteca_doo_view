class Aluno:
    def __init__(self, matricula: str, nome: str):
        self.__matricula = matricula
        self.__nome = nome

    @property
    def matricula(self):
        return self.__matricula

    @property
    def nome(self):
        return self.__nome

    @nome.setter
    def nome(self, nome):
        self.__nome = nome
