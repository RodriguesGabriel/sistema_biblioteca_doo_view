from modelo.aluno import Aluno


class Turma:
    def __init__(self, id: str, chave: str):
        self.__id = id
        self.__chave = chave
        self.__alunos = []

    @property
    def id(self):
        return self.__id

    @property
    def chave(self):
        return self.__chave

    @chave.setter
    def chave(self, chave: str):
        self.__chave = chave

    @property
    def alunos(self):
        return self.__alunos

    @alunos.setter
    def alunos(self, alunos: []):
        self.__alunos = alunos

    def adicionar_aluno(self, aluno: Aluno):
        if type(aluno) is not Aluno:
            pass

        self.__alunos.append(aluno)

