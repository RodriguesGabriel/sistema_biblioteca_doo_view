from controle.controlador_aluno import ControladorAluno
from controle.controlador_autor import ControladorAutor
from controle.controlador_editora import ControladorEditora
from controle.controlador_emprestimo import ControladorEmprestimo
from controle.controlador_livro import ControladorLivro
from controle.controlador_turma import ControladorTurma
from limite.tela_biblioteca import TelaBiblioteca


class ControladorBiblioteca:
    __intance = None

    def __init__(self):
        self.__controlador_aluno = ControladorAluno(self)
        self.__controlador_turma = ControladorTurma(self)
        self.__controlador_editora = ControladorEditora(self)
        self.__controlador_autor = ControladorAutor(self)
        self.__controlador_emprestimo = ControladorEmprestimo(self)
        self.__tela_biblioteca = TelaBiblioteca(self)
        self.__controlador_livro = ControladorLivro(self)

    def __new__(cls):
        if ControladorBiblioteca.__intance is None:
            ControladorBiblioteca.__intance = object.__new__(cls)
        return ControladorBiblioteca.__intance

    def inicializa_sistema(self):
        self.abre_tela()

    @property
    def controlador_livro(self):
        return self.__controlador_livro

    @property
    def controlador_aluno(self):
        return self.__controlador_aluno

    @property
    def controlador_turma(self):
        return self.__controlador_turma

    @property
    def controlador_autor(self):
        return self.__controlador_autor

    @property
    def controlador_editora(self):
        return self.__controlador_editora

    @property
    def controlador_emprestimo(self):
        return self.__controlador_emprestimo

    def encerra_sistema(self):
        self.__tela_biblioteca.close()
        exit(0)

    def abre_tela(self):
        lista_opcoes = {"bt_livro": self.controlador_livro.abrir_tela_principal,
                        "bt_aluno": self.controlador_aluno.abrir_tela_principal,
                        "bt_turma": self.controlador_turma.abrir_tela_principal,
                        "bt_editora": self.controlador_editora.abrir_tela_principal,
                        "bt_autor": self.controlador_autor.abrir_tela_principal,
                        "bt_emprestimo": self.controlador_emprestimo.abrir_tela_principal,
                        "Sair": self.encerra_sistema}

        (botao, dados) = self.__tela_biblioteca.mostrar_menu()

        funcao_escolhida = lista_opcoes[botao]

        funcao_escolhida()

