from dao.editoraDAO import EditoraDAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from limite.tela_editora_principal import TelaEditoraPrincipal
from limite.tela_editora_detalhe import TelaEditoraDetalhe
from modelo.editora import Editora


class ControladorEditora:

    def __init__(self, controlador_biblioteca):
        self.__controlador_biblioteca = controlador_biblioteca
        self.__tela_editora_principal = TelaEditoraPrincipal(self)
        self.__tela_editora_detalhe = TelaEditoraDetalhe(self)
        self.__dao = EditoraDAO()

    def cadastrar(self, dados):
        editora = Editora(dados.get('id'),
                          dados.get('nome'))

        if self.__dao.get(editora.id):
            raise RequisicaoMalFeita('uma editora já está cadastrada com este id!')

        self.__dao.add(editora)

        self.__tela_editora_principal.mostrar_mensagem("Sucesso", "Cadastrado com sucesso!")

        return self.detalhar(id=editora.id)

    def excluir(self, id):
        livros = self.__controlador_biblioteca.controlador_livro.listar()
        for livro in livros:
            if livro.get('Editora').get('id') == id:
                raise RequisicaoMalFeita("não é possível deletar esta editora\npois existe um livro cadastrado com ela")

        self.__dao.remove(id)
        self.__tela_editora_principal.mostrar_mensagem("Sucesso", "Excluido com sucesso!")

    def alterar(self, dados):
        editora = self.__dao.get(dados.get('id'))

        if not editora:
            raise RequisicaoMalFeita("editora com o id informado não existe")

        editora = Editora(dados.get('id'),
                          dados.get('nome'))

        self.__dao.add(editora)

        self.__tela_editora_principal.mostrar_mensagem("Sucesso", "Atualizado com sucesso!")

        return self.detalhar(id=editora.id)

    def listar(self):
        editoraes = self.__dao.get_all()
        editoraes_dados = []
        for editora in editoraes:
            dados = {"Id": editora.id,
                     "Nome": editora.nome}

            editoraes_dados.append(dados)

        return editoraes_dados

    def detalhar(self, id: str, gerar_erro=False):
        editora = self.__dao.get(id)

        editora_dados = {
            "Id": editora.id,
            "Nome": editora.nome
        }

        if gerar_erro and not editora:
            raise RequisicaoMalFeita("erro: o id da editora informado não existe")

        return editora_dados

    def pegar_editora_pelo_id(self, id: str, gerar_erro=False):
        editora = self.__dao.get(id)

        if gerar_erro and not editora:
            raise RequisicaoMalFeita("erro: o id da editora informado não existe")

        return editora

    def abrir_tela_detalhe(self, id=None):
        lista_opcoes = {"bt_novo": self.cadastrar,
                        "bt_excluir": self.excluir,
                        "bt_alterar": self.alterar,
                        "Voltar": self.abrir_tela_principal,
                        "Sair": self.__controlador_biblioteca.encerra_sistema}

        editora_dados = None
        if id:
            editora_dados = self.detalhar(id)

        while True:
            try:
                botao, dados = self.__tela_editora_detalhe.mostrar_menu(editora_dados)
                funcao_escolhida = lista_opcoes[botao]
                if botao in ['Voltar', 'Sair']:
                    funcao_escolhida()

                if botao == 'bt_excluir':
                    funcao_escolhida(dados.get('id'))
                    editora_dados = None

                if botao in ['bt_novo', 'bt_alterar']:
                    editora_dados = funcao_escolhida(dados)

            except RequisicaoMalFeita as erro:
                self.__tela_editora_principal.mostrar_mensagem("erro", erro.messagem)
            except Exception as erro:
                print(erro)
                self.__tela_editora_principal.mostrar_mensagem("erro", "Um erro inexperado ocorreu!!!")

    def abrir_tela_principal(self):
        lista_opcoes = {"bt_novo": self.abrir_tela_detalhe,
                        "bt_alterar": self.abrir_tela_detalhe,
                        "bt_excluir": self.excluir,
                        "Voltar": self.__controlador_biblioteca.inicializa_sistema,
                        "Sair": self.__controlador_biblioteca.encerra_sistema}

        while True:
            try:
                editoraes = self.listar()

                botao, id = self.__tela_editora_principal.mostrar_menu(editoraes)
                funcao_escolhida = lista_opcoes[botao]

                if botao in ['bt_alterar', 'bt_excluir']:
                    funcao_escolhida(id)
                else:
                    funcao_escolhida()
            except RequisicaoMalFeita as erro:
                self.__tela_editora_principal.mostrar_mensagem("erro", erro.messagem)
            except Exception as erro:
                print(erro)
                self.__tela_editora_principal.mostrar_mensagem("erro", "Um erro inexperado ocorreu!!!")
