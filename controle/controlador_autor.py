from dao.autorDAO import AutorDAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from limite.tela_autor_principal import TelaAutorPrincipal
from limite.tela_autor_detalhe import TelaAutorDetalhe
from modelo.autor import Autor


class ControladorAutor:

    def __init__(self, controlador_biblioteca):
        self.__controlador_biblioteca = controlador_biblioteca
        self.__tela_autor_principal = TelaAutorPrincipal(self)
        self.__tela_autor_detalhe = TelaAutorDetalhe(self)
        self.__dao = AutorDAO()

    def cadastrar(self, dados):
        autor = Autor(dados.get('id'),
                      dados.get('nome'))

        if self.__dao.get(autor.id):
            raise RequisicaoMalFeita('um autor já está cadastrado com este id!')

        self.__dao.add(autor)

        self.__tela_autor_principal.mostrar_mensagem("Sucesso", "Cadastrado com sucesso!")

        return self.detalhar(id=autor.id)

    def excluir(self, id):
        livros = self.__controlador_biblioteca.controlador_livro.listar()
        for livro in livros:
            if livro.get('Autor').get('id') == id:
                raise RequisicaoMalFeita("não é possível deletar este autor\npois existe um livro cadastrado com ele")

        self.__dao.remove(id)
        self.__tela_autor_principal.mostrar_mensagem("Sucesso", "Excluido com sucesso!")

    def alterar(self, dados):
        autor = self.__dao.get(dados.get('id'))

        if not autor:
            raise RequisicaoMalFeita("autor com o id informado não existe")

        autor = Autor(dados.get('id'),
                      dados.get('nome'))

        self.__dao.add(autor)

        self.__tela_autor_principal.mostrar_mensagem("Sucesso", "Atualizado com sucesso!")

        return self.detalhar(id=autor.id)

    def listar(self):
        autores = self.__dao.get_all()
        autores_dados = []
        for autor in autores:
            dados = {"Id": autor.id,
                     "Nome": autor.nome}

            autores_dados.append(dados)

        return autores_dados

    def detalhar(self, id: str, gerar_erro=False):
        autor = self.__dao.get(id)

        autor_dados = {
            "Id": autor.id,
            "Nome": autor.nome
        }

        if gerar_erro and not autor:
            raise RequisicaoMalFeita("erro: o id do autor informado não existe")

        return autor_dados

    def pegar_autor_pelo_id(self, id: str, gerar_erro=False):
        autor = self.__dao.get(id)

        if gerar_erro and not autor:
            raise RequisicaoMalFeita("erro: o id do autor informado não existe")

        return autor

    def abrir_tela_detalhe(self, id=None):
        lista_opcoes = {"bt_novo": self.cadastrar,
                        "bt_excluir": self.excluir,
                        "bt_alterar": self.alterar,
                        "Voltar": self.abrir_tela_principal,
                        "Sair": self.__controlador_biblioteca.encerra_sistema}

        autor_dados = None
        if id:
            autor_dados = self.detalhar(id)

        while True:
            try:
                botao, dados = self.__tela_autor_detalhe.mostrar_menu(autor_dados)
                funcao_escolhida = lista_opcoes[botao]
                if botao in ['Voltar', 'Sair']:
                    funcao_escolhida()

                if botao == 'bt_excluir':
                    funcao_escolhida(dados.get('id'))
                    autor_dados = None

                if botao in ['bt_novo', 'bt_alterar']:
                    autor_dados = funcao_escolhida(dados)

            except RequisicaoMalFeita as erro:
                self.__tela_autor_principal.mostrar_mensagem("erro", erro.messagem)
            except Exception as erro:
                print(erro)
                self.__tela_autor_principal.mostrar_mensagem("erro", "Um erro inexperado ocorreu!!!")

    def abrir_tela_principal(self):
        lista_opcoes = {"bt_novo": self.abrir_tela_detalhe,
                        "bt_alterar": self.abrir_tela_detalhe,
                        "bt_excluir": self.excluir,
                        "Voltar": self.__controlador_biblioteca.inicializa_sistema,
                        "Sair": self.__controlador_biblioteca.encerra_sistema}

        while True:
            try:
                autores = self.listar()

                botao, id = self.__tela_autor_principal.mostrar_menu(autores)
                funcao_escolhida = lista_opcoes[botao]

                if botao in ['bt_alterar', 'bt_excluir']:
                    funcao_escolhida(id)
                else:
                    funcao_escolhida()
            except RequisicaoMalFeita as erro:
                self.__tela_autor_principal.mostrar_mensagem("erro", erro.messagem)
            except Exception as erro:
                print(erro)
                self.__tela_autor_principal.mostrar_mensagem("erro", "Um erro inexperado ocorreu!!!")
