from dao.livroDAO import LivroDAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from limite.tela_livro_principal import TelaLivroPrincipal
from limite.tela_livro_detalhe import TelaLivroDetalhe
from modelo.livro import Livro


class ControladorLivro:

    def __init__(self, controlador_biblioteca):
        self.__controlador_biblioteca = controlador_biblioteca
        self.__tela_livro_principal = TelaLivroPrincipal(self)
        self.__tela_livro_detalhe = TelaLivroDetalhe(self)
        self.__dao = LivroDAO()

    def cadastrar(self, dados):

        autor = self.__controlador_biblioteca.controlador_autor.pegar_autor_pelo_id(dados.get('autor').get('Id'),
                                                                                    gerar_erro=True)
        editora = self.__controlador_biblioteca.controlador_editora.pegar_editora_pelo_id(dados.get('editora').get('Id'),
                                                                                          gerar_erro=True)

        livro = Livro(dados.get('registro'),
                      dados.get('titulo'),
                      autor,
                      dados.get('categoria'),
                      dados.get('descricao'),
                      editora,
                      dados.get('ativo'),
                      dados.get('observacao'))

        if self.__dao.get(livro.registro):
            raise RequisicaoMalFeita('um livro já está cadastrado com este registro!')

        self.__dao.add(livro)

        self.__tela_livro_principal.mostrar_mensagem("Sucesso", "Cadastrado com sucesso!")

        return self.detalhar(registro=livro.registro)

    def excluir(self, registro):
        self.__dao.remove(registro)
        self.__tela_livro_principal.mostrar_mensagem("Sucesso", "Excluido com sucesso!")

    def alterar(self, dados):
        livro = self.__dao.get(dados.get('registro'))

        if not livro:
            raise RequisicaoMalFeita("livro com o registro informado não existe")

        autor = self.__controlador_biblioteca.controlador_autor.pegar_autor_pelo_id(dados.get('autor').get('Id'),
                                                                                    gerar_erro=True)
        editora = self.__controlador_biblioteca.controlador_editora.pegar_editora_pelo_id(dados.get('editora').get('Id'),
                                                                                          gerar_erro=True)

        livro = Livro(dados.get('registro'),
                      dados.get('titulo'),
                      autor,
                      dados.get('categoria'),
                      dados.get('descricao'),
                      editora,
                      dados.get('ativo'),
                      dados.get('observacao'))

        self.__dao.add(livro)

        self.__tela_livro_principal.mostrar_mensagem("Sucesso", "Atualizado com sucesso!")

        return self.detalhar(registro=livro.registro)

    def listar(self):
        livros = self.__dao.get_all()
        livros_dados = []
        for livro in livros:
            dados = {
                "Registro": livro.registro,
                "Título": livro.titulo,
                "Autor": {"id": livro.autor.id,
                          "nome": livro.autor.nome},
                "Categoria": livro.categora,
                "Descricao": livro.descricao,
                "Editora": {"id": livro.editora.id,
                            "nome": livro.editora.nome},
                "Ativo": livro.ativo,
                "Disponível": livro.disponivel,
                "Observação": livro.observacao
            }

            livros_dados.append(dados)

        return livros_dados

    def detalhar(self, registro: str, gerar_erro=False):
        livro = self.__dao.get(registro)

        livro_dados = {
            "Registro": livro.registro,
            "Título": livro.titulo,
            "Autor": {"id": livro.autor.id,
                      "nome": livro.autor.nome},
            "Categoria": livro.categora,
            "Descricao": livro.descricao,
            "Editora": {"id": livro.editora.id,
                        "nome": livro.editora.nome},
            "Ativo": livro.ativo,
            "Disponível": livro.disponivel,
            "Observação": livro.observacao
        }

        if gerar_erro and not livro:
            raise RequisicaoMalFeita("erro: o registro do livro informado não existe")

        return livro_dados

    def pegar_livro_pelo_registro(self, registro: str, gerar_erro=False):
        livro = self.__dao.get(registro)

        if gerar_erro and not livro:
            raise RequisicaoMalFeita("erro: o registro do livro informado não existe")

        return livro

    def abrir_tela_detalhe(self, registro=None):
        lista_opcoes = {"bt_novo": self.cadastrar,
                        "bt_excluir": self.excluir,
                        "bt_alterar": self.alterar,
                        "Voltar": self.abrir_tela_principal,
                        "Sair": self.__controlador_biblioteca.encerra_sistema}

        livro_dados = None
        if registro:
            livro_dados = self.detalhar(registro)

        autores_dados = self.__controlador_biblioteca.controlador_autor.listar()
        editoras_dados = self.__controlador_biblioteca.controlador_editora.listar()

        if not autores_dados:
            raise RequisicaoMalFeita("Não é possível cadastrar nenhum livro, pois não existem autores cadastrados")
        if not editoras_dados:
            raise RequisicaoMalFeita("Não é possível cadastrar nenhum livro, pois não existem editoras cadastradas")

        while True:
            try:
                botao, dados = self.__tela_livro_detalhe.mostrar_menu(autores_dados, editoras_dados, livro_dados)
                funcao_escolhida = lista_opcoes[botao]
                if botao in ['Voltar', 'Sair']:
                    funcao_escolhida()

                if botao == 'bt_excluir':
                    funcao_escolhida(dados.get('registro'))
                    livro_dados = None

                if botao in ['bt_novo', 'bt_alterar']:
                    livro_dados = funcao_escolhida(dados)

            except RequisicaoMalFeita as erro:
                self.__tela_livro_principal.mostrar_mensagem("erro", erro.messagem)
            except Exception as erro:
                print(erro)
                self.__tela_livro_principal.mostrar_mensagem("erro", "Um erro inexperado ocorreu!!!")

    def abrir_tela_principal(self):
        lista_opcoes = {"bt_novo": self.abrir_tela_detalhe,
                        "bt_alterar": self.abrir_tela_detalhe,
                        "bt_excluir": self.excluir,
                        "Voltar": self.__controlador_biblioteca.inicializa_sistema,
                        "Sair": self.__controlador_biblioteca.encerra_sistema}

        while True:
            try:
                livros = self.listar()

                botao, registro = self.__tela_livro_principal.mostrar_menu(livros)
                funcao_escolhida = lista_opcoes[botao]

                if botao in ['bt_alterar', 'bt_excluir']:
                    funcao_escolhida(registro)
                else:
                    funcao_escolhida()
            except RequisicaoMalFeita as erro:
                self.__tela_livro_principal.mostrar_mensagem("erro", erro.messagem)
            except Exception as erro:
                print(erro)
                self.__tela_livro_principal.mostrar_mensagem("erro", "Um erro inexperado ocorreu!!!")
