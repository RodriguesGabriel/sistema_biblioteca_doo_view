from dateutil.utils import today

from dao.emprestimoDAO import EmprestimoDAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from limite.tela_emprestimo_principal import TelaEmprestimoPrincipal
from limite.tela_emprestimo_detalhe import TelaEmprestimoDetalhe
from modelo.emprestimo import Emprestimo


class ControladorEmprestimo:

    def __init__(self, controlador_biblioteca):
        self.__controlador_biblioteca = controlador_biblioteca
        self.__tela_emprestimo_principal = TelaEmprestimoPrincipal(self)
        self.__tela_emprestimo_detalhe = TelaEmprestimoDetalhe(self)
        self.__dao = EmprestimoDAO()

    def cadastrar(self, dados):

        aluno = self.__controlador_biblioteca.controlador_aluno.pegar_aluno_pelo_matricula(dados.get('aluno').get('Matricula'),
                                                                                           gerar_erro=True)
        livro = self.__controlador_biblioteca.controlador_livro.pegar_livro_pelo_registro(dados.get('livro').get('Registro'),
                                                                                          gerar_erro=True)
        
        if not livro.disponivel:
            raise RequisicaoMalFeita('este livro já está emprestado!')
        
        if not livro.ativo:
            raise RequisicaoMalFeita ('este livro está desativado!')

        if self.aluno_com_emprestimo_pendente(aluno.matricula):
            raise RequisicaoMalFeita('Este aluno possui um empréstimo pendente!')

        emprestimo = Emprestimo(id=dados.get('id'),
                                aluno=aluno,
                                livro=livro,
                                data_do_emprestimo=today(),
                                data_limite=dados.get('data_limite'))

        if self.__dao.get(emprestimo.id):
            raise RequisicaoMalFeita('um emprestimo já está cadastrado com este registro!')

        livro.disponivel = False
        self.__dao.add(emprestimo)

        self.__tela_emprestimo_principal.mostrar_mensagem("Sucesso", "Cadastrado com sucesso!")

        return self.detalhar(id=emprestimo.id)

    def excluir(self, id:str):
        registro = self.__dao.get(id)

        livro = self.__controlador_biblioteca.controlador_livro.pegar_livro_pelo_registro(registro.livro.registro,
                                                                                          gerar_erro=True)
        livro.disponivel = True
        self.__dao.remove(id)
        self.__tela_emprestimo_principal.mostrar_mensagem("Sucesso", "Excluido com sucesso!")

    def alterar(self, dados):
        emprestimo = self.__dao.get(dados.get('id'))

        if not emprestimo:
            raise RequisicaoMalFeita("emprestimo com o id informado não existe")

        aluno = self.__controlador_biblioteca.controlador_aluno.pegar_aluno_pelo_matricula(dados.get ('aluno').get ('Matricula'),
                                                                                            gerar_erro=True)
        livro = self.__controlador_biblioteca.controlador_livro.pegar_livro_pelo_registro(dados.get ('livro').get ('Registro'),
                                                                                          gerar_erro=True)

        emprestimo = Emprestimo (id=dados.get('id'),
                                 aluno=aluno,
                                 livro=livro,
                                 data_do_emprestimo=dados.get('data_limite'),
                                 data_limite=dados.get('data_limite'))

        self.__dao.add(emprestimo)

        self.__tela_emprestimo_principal.mostrar_mensagem("Sucesso", "Atualizado com sucesso!")

        return self.detalhar(id=emprestimo.id)

    def listar(self):
        emprestimos = self.__dao.get_all()
        emprestimos_dados = []
        for emprestimo in emprestimos:
            dados = {
                "ID": emprestimo.id,
                "Aluno": {"matricula": emprestimo.aluno.matricula,
                          "nome": emprestimo.aluno.nome},
                "Livro": {"registro": emprestimo.livro.registro,
                           "titulo": emprestimo.livro.titulo},
                "Data Do Emprestimo": str(emprestimo.data_do_emprestimo),
                "Data limite": str(emprestimo.data_limite),
                "Entregue": emprestimo.entregue
            }

            emprestimos_dados.append(dados)

        return emprestimos_dados

    def detalhar(self, id: str, gerar_erro=False):
        emprestimo = self.__dao.get(id)

        dados = {
                "ID": emprestimo.id,
                "Aluno": {"matricula": emprestimo.aluno.matricula,
                          "nome": emprestimo.aluno.nome},
                "Livro": {"registro": emprestimo.livro.registro,
                          "titulo": emprestimo.livro.titulo},
                "Data Do Emprestimo": str (emprestimo.data_do_emprestimo),
                "Data limite": str (emprestimo.data_limite),
                "Entregue": emprestimo.entregue
        }

        if gerar_erro and not emprestimo:
            raise RequisicaoMalFeita("erro: o registro do emprestimo informado não existe")

        return dados

    def aluno_com_emprestimo_pendente(self, matricula: str):
        emprestimos = self.__dao.get_all()
        for emprestimo in emprestimos:
            if emprestimo.aluno.matricula == matricula and not emprestimos.entregue:
                return True

    def abrir_tela_detalhe(self, registro=None):
        lista_opcoes = {"bt_novo": self.cadastrar,
                        "bt_excluir": self.excluir,
                        "bt_alterar": self.alterar,
                        "Voltar": self.abrir_tela_principal,
                        "Sair": self.__controlador_biblioteca.encerra_sistema}

        emprestimo_dados = None
        if registro:
            emprestimo_dados = self.detalhar(registro)

        alunos_dados = self.__controlador_biblioteca.controlador_aluno.listar()
        livros_dados = self.__controlador_biblioteca.controlador_livro.listar()

        if not alunos_dados:
            raise RequisicaoMalFeita("Não é possível cadastrar nenhum emprestimo, pois não existem alunos cadastrados")
        if not livros_dados:
            raise RequisicaoMalFeita("Não é possível cadastrar nenhum emprestimo, pois não existem livros cadastradas")

        while True:
            try:
                botao, dados = self.__tela_emprestimo_detalhe.mostrar_menu(alunos_dados, livros_dados, emprestimo_dados)
                funcao_escolhida = lista_opcoes[botao]
                if botao in ['Voltar', 'Sair']:
                    funcao_escolhida()

                if botao == 'bt_excluir':
                    funcao_escolhida(dados.get('id'))
                    emprestimo_dados = None

                if botao in ['bt_novo', 'bt_alterar']:
                    emprestimo_dados = funcao_escolhida(dados)

            except RequisicaoMalFeita as erro:
                self.__tela_emprestimo_principal.mostrar_mensagem("erro", erro.messagem)
            except Exception as erro:
                print(erro)
                self.__tela_emprestimo_principal.mostrar_mensagem("erro", "Um erro inexperado ocorreu!!!")

    def abrir_tela_principal(self):
        lista_opcoes = {"bt_novo": self.abrir_tela_detalhe,
                        "bt_alterar": self.abrir_tela_detalhe,
                        "bt_excluir": self.excluir,
                        "Voltar": self.__controlador_biblioteca.inicializa_sistema,
                        "Sair": self.__controlador_biblioteca.encerra_sistema}

        while True:
            try:
                emprestimos = self.listar()

                botao, id = self.__tela_emprestimo_principal.mostrar_menu(emprestimos)
                funcao_escolhida = lista_opcoes[botao]

                if botao in ['bt_alterar', 'bt_excluir']:
                    funcao_escolhida(id)
                else:
                    funcao_escolhida()
            except RequisicaoMalFeita as erro:
                self.__tela_emprestimo_principal.mostrar_mensagem("erro", erro.messagem)
            except Exception as erro:
                print(erro)
                self.__tela_emprestimo_principal.mostrar_mensagem("erro", "Um erro inexperado ocorreu!!!")
