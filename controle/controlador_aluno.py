from dao.alunoDAO import AlunoDAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from limite.tela_aluno_principal import TelaAlunoPrincipal
from limite.tela_aluno_detalhe import TelaAlunoDetalhe
from modelo.aluno import Aluno


class ControladorAluno:

    def __init__(self, controlador_biblioteca):
        self.__controlador_biblioteca = controlador_biblioteca
        self.__tela_aluno_principal = TelaAlunoPrincipal(self)
        self.__tela_aluno_detalhe = TelaAlunoDetalhe(self)
        self.__dao = AlunoDAO()

    def cadastrar(self, dados):
        aluno = Aluno(dados.get('matricula'),
                      dados.get('nome'))

        if self.__dao.get(aluno.matricula):
            raise RequisicaoMalFeita('um aluno já está cadastrado com esta matrícula!')

        turma = self.__controlador_biblioteca.controlador_turma.pegar_turma_pelo_id(dados.get('turma').get('Id'),
                                                                                    gerar_erro=True)
        self.__controlador_biblioteca.controlador_turma.vincular_aluno(aluno, turma.id)

        self.__dao.add(aluno)

        self.__tela_aluno_principal.mostrar_mensagem("Sucesso", "Cadastrado com sucesso!")

        return self.detalhar(matricula=aluno.matricula)

    def excluir(self, matricula):
        self.__controlador_biblioteca.controlador_turma.desvincular_aluno(matricula)

        self.__dao.remove(matricula)
        self.__tela_aluno_principal.mostrar_mensagem("Sucesso", "Excluido com sucesso!")

    def alterar(self, dados):
        aluno = self.__dao.get(dados.get('matricula'))

        if not aluno:
            raise RequisicaoMalFeita("aluno com a matricula informado não existe")

        self.__controlador_biblioteca.controlador_turma.desvincular_aluno(dados.get('matricula'))

        aluno = Aluno(dados.get('matricula'),
                      dados.get('nome'))

        self.__controlador_biblioteca.controlador_turma.vincular_aluno(aluno, dados.get('turma').get('Id'))

        self.__dao.add(aluno)

        self.__tela_aluno_principal.mostrar_mensagem("Sucesso", "Atualizado com sucesso!")

        return self.detalhar(matricula=aluno.matricula)

    def listar(self):
        alunos = self.__dao.get_all()
        alunos_dados = []
        for aluno in alunos:

            turmas = self.__controlador_biblioteca.controlador_turma.listar()
            turma_vinculada = None

            for turma in turmas:
                for aluno_turma in turma.get('Alunos'):
                    if aluno_turma.get('matricula') == aluno.matricula:
                        turma_vinculada = turma

            dados = {"Matricula": aluno.matricula,
                     "Nome": aluno.nome,
                     "Turma": turma_vinculada}

            alunos_dados.append(dados)

        return alunos_dados

    def detalhar(self, matricula: str, gerar_erro=False):
        aluno = self.__dao.get(matricula)

        turmas = self.__controlador_biblioteca.controlador_turma.listar()
        turma_vinculada = None
        for turma in turmas:
            for aluno_turma in turma.get('Alunos'):
                if aluno_turma.get('matricula') == aluno.matricula:
                    turma_vinculada = turma

        aluno_dados = {
            "Matricula": aluno.matricula,
            "Nome": aluno.nome,
            "Turma": turma_vinculada
        }

        if gerar_erro and not aluno:
            raise RequisicaoMalFeita("erro: o matricula do aluno informado não existe")

        return aluno_dados

    def pegar_aluno_pelo_matricula(self, matricula: str, gerar_erro=False):
        aluno = self.__dao.get(matricula)

        if gerar_erro and not aluno:
            raise RequisicaoMalFeita("erro: o matricula do aluno informado não existe")

        return aluno

    def abrir_tela_detalhe(self, matricula=None):
        lista_opcoes = {"bt_novo": self.cadastrar,
                        "bt_excluir": self.excluir,
                        "bt_alterar": self.alterar,
                        "Voltar": self.abrir_tela_principal,
                        "Sair": self.__controlador_biblioteca.encerra_sistema}

        aluno_dados = None
        if matricula:
            aluno_dados = self.detalhar(matricula)

        turmas_dados = self.__controlador_biblioteca.controlador_turma.listar()
        if not turmas_dados:
            raise RequisicaoMalFeita("Não é possível cadastrar nenhum aluno, pois não existem turmas cadastradas")

        while True:
            try:
                turmas_dados = self.__controlador_biblioteca.controlador_turma.listar()
                botao, dados = self.__tela_aluno_detalhe.mostrar_menu(turmas_dados, aluno_dados)
                funcao_escolhida = lista_opcoes[botao]
                if botao in ['Voltar', 'Sair']:
                    funcao_escolhida()

                if botao == 'bt_excluir':
                    funcao_escolhida(dados.get('matricula'))
                    aluno_dados = None

                if botao in ['bt_novo', 'bt_alterar']:
                    aluno_dados = funcao_escolhida(dados)

            except RequisicaoMalFeita as erro:
                self.__tela_aluno_principal.mostrar_mensagem("erro", erro.messagem)

    def abrir_tela_principal(self):
        lista_opcoes = {"bt_novo": self.abrir_tela_detalhe,
                        "bt_alterar": self.abrir_tela_detalhe,
                        "bt_excluir": self.excluir,
                        "Voltar": self.__controlador_biblioteca.inicializa_sistema,
                        "Sair": self.__controlador_biblioteca.encerra_sistema}

        while True:
            try:
                alunos = self.listar()

                botao, matricula = self.__tela_aluno_principal.mostrar_menu(alunos)
                funcao_escolhida = lista_opcoes[botao]

                if botao in ['bt_alterar', 'bt_excluir']:
                    funcao_escolhida(matricula)
                else:
                    funcao_escolhida()
            except RequisicaoMalFeita as erro:
                self.__tela_aluno_principal.mostrar_mensagem("erro", erro.messagem)
