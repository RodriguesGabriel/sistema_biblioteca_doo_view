from dao.turmaDAO import TurmaDAO
from excecao.requisicao_mal_feita import RequisicaoMalFeita
from limite.tela_turma_principal import TelaTurmaPrincipal
from limite.tela_turma_detalhe import TelaTurmaDetalhe
from modelo.turma import Turma


class ControladorTurma:

    def __init__(self, controlador_biblioteca):
        self.__controlador_biblioteca = controlador_biblioteca
        self.__tela_turma_principal = TelaTurmaPrincipal(self)
        self.__tela_turma_detalhe = TelaTurmaDetalhe(self)
        self.__dao = TurmaDAO()

    def cadastrar(self, dados):
        turma = Turma(dados.get('id'),
                      dados.get('chave'))

        if self.__dao.get(turma.id):
            raise RequisicaoMalFeita('uma turma já está cadastrado com este id!')

        self.__dao.add(turma)

        self.__tela_turma_principal.mostrar_mensagem("Sucesso", "Cadastrada com sucesso!")

        return self.detalhar(id=turma.id)

    def excluir(self, id):
        turma = self.__dao.get(id)
        if len(turma.alunos) > 0:
            raise RequisicaoMalFeita("não é possível deletar esta turma\npois existem alunos vinculados nela")

        self.__dao.remove(id)
        self.__tela_turma_principal.mostrar_mensagem("Sucesso", "Excluida com sucesso!")

    def alterar(self, dados):
        turma = self.__dao.get(dados.get('id'))

        if not turma:
            raise RequisicaoMalFeita("turma com o id informado não existe")

        turma = Turma(dados.get('id'),
                      dados.get('chave'))

        self.__dao.add(turma)

        self.__tela_turma_principal.mostrar_mensagem("Sucesso", "Atualizada com sucesso!")

        return self.detalhar(id=turma.id)

    def listar(self):
        turmas = self.__dao.get_all()
        turmas_dados = []
        for turma in turmas:
            alunos = []
            for aluno in turma.alunos:
                dados = {"matricula": aluno.matricula,
                         "nome": aluno.nome}
                alunos.append (dados)

            dados = {"Id": turma.id,
                     "Chave": turma.chave,
                     "Total de Alunos": len(turma.alunos),
                     "Alunos": alunos}

            turmas_dados.append(dados)

        return turmas_dados

    def detalhar(self, id: str, gerar_erro=False):
        turma = self.__dao.get(id)

        alunos = []
        for aluno in turma.alunos:
            dados = {"matricula": aluno.matricula,
                     "nome": aluno.nome}
            alunos.append(dados)

        turma_dados = {
            "Id": turma.id,
            "Chave": turma.chave,
            "Alunos": alunos
        }

        if gerar_erro and not turma:
            raise RequisicaoMalFeita("erro: o id da turma informada não existe")

        return turma_dados

    def pegar_turma_pelo_id(self, id: str, gerar_erro=False):
        turma = self.__dao.get(id)

        if gerar_erro and not turma:
            raise RequisicaoMalFeita("erro: o id da turma informada não existe")

        return turma

    def vincular_aluno(self, aluno, turma_id):
        turma = self.__dao.get(turma_id)

        if not turma:
            raise RequisicaoMalFeita("turma com o id informado não existe")

        for turma_aluno in turma.alunos:
            if turma_aluno.matricula == aluno.matricula:
                return

        turma.alunos.append(aluno)

        self.__dao.add(turma)

    def desvincular_aluno(self, aluno_matricula):
        turmas = self.__dao.get_all()

        for turma in turmas:
            for turma_aluno in turma.alunos:
                if turma_aluno.matricula == aluno_matricula:
                    turma.alunos.remove(turma_aluno)
                    self.__dao.add(turma)
                    return

    def abrir_tela_detalhe(self, id=None):
        lista_opcoes = {"bt_novo": self.cadastrar,
                        "bt_excluir": self.excluir,
                        "bt_alterar": self.alterar,
                        "Voltar": self.abrir_tela_principal,
                        "Sair": self.__controlador_biblioteca.encerra_sistema}

        turma_dados = None
        if id:
            turma_dados = self.detalhar(id)

        while True:
            try:
                botao, dados = self.__tela_turma_detalhe.mostrar_menu(turma_dados)
                funcao_escolhida = lista_opcoes[botao]
                if botao in ['Voltar', 'Sair']:
                    funcao_escolhida()

                if botao == 'bt_excluir':
                    funcao_escolhida(dados.get('id'))
                    turma_dados = None

                if botao in ['bt_novo', 'bt_alterar']:
                    turma_dados = funcao_escolhida(dados)

            except RequisicaoMalFeita as erro:
                self.__tela_turma_principal.mostrar_mensagem("erro", erro.messagem)
            except Exception as erro:
                print(erro)
                self.__tela_turma_principal.mostrar_mensagem("erro", "Um erro inexperado ocorreu!!!")


    def abrir_tela_principal(self):
            lista_opcoes = {"bt_novo": self.abrir_tela_detalhe,
                            "bt_alterar": self.abrir_tela_detalhe,
                            "bt_excluir": self.excluir,
                            "Voltar": self.__controlador_biblioteca.inicializa_sistema,
                            "Sair": self.__controlador_biblioteca.encerra_sistema}

            while True:
                try:
                    turmas = self.listar()

                    botao, id = self.__tela_turma_principal.mostrar_menu(turmas)
                    funcao_escolhida = lista_opcoes[botao]

                    if botao in ['bt_alterar', 'bt_excluir']:
                        funcao_escolhida(id)
                    else:
                        funcao_escolhida()
                except RequisicaoMalFeita as erro:
                    self.__tela_turma_principal.mostrar_mensagem("erro", erro.messagem)
                except Exception as erro:
                    print(erro)
                    self.__tela_turma_principal.mostrar_mensagem("erro", "Um erro inexperado ocorreu!!!")
