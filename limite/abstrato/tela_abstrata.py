from abc import ABC, abstractmethod
import PySimpleGUI as sg


class TelaAbstrata(ABC):

    def __init__(self, controlador):
        self.__controlador = controlador
        self.__window = sg.Window('title')

    @property
    def window(self):
        return self.__window

    @property
    def controlador(self):
        return self.__controlador

    @window.setter
    def window(self, window):
        self.__window = window

    @abstractmethod
    def iniciar_nova_janela(self):
        self.close()
        self.__window = sg.Window('Sistema Biblioteca', finalize=True, size=(800, 750), element_justification='center')

    @abstractmethod
    def close(self):
        self.__window.close()

    @abstractmethod
    def mostrar_mensagem(self, titulo:str, mensagem:str):
        sg.popup(titulo, mensagem)
