from limite.abstrato.tela_abstrata import TelaAbstrata
import PySimpleGUI as sg


class TelaEditoraDetalhe(TelaAbstrata):

    def init_components(self, dados=None):
        menu_def = [
            ['&Sistema', ['Sair', 'Voltar']],
        ]

        bloquear_campo_reg = True if dados else None

        dados = dados if dados else {}

        frame_layout = [[sg.Text('Id:', justification='left', size=(20, 1))],
                        [sg.InputText(disabled=bloquear_campo_reg, default_text=dados.get('Id'), key='id',
                                      justification='left', size=(10, 1))],

                        [sg.Text('Nome:', justification='left', size=(60, 1))],
                        [sg.InputText(default_text=dados.get('Nome'), size=(50, 1), key='nome')]]

        frame_principal = sg.Frame('editora', layout=frame_layout, element_justification='left')

        layout = [[sg.MenuBar(menu_def)],
                  [sg.Text('Controle dos Editoraes', justification='center', size=(40, 1))],
                  [frame_principal],
                  [sg.Button('Adicionar', key='bt_novo', size=(20, 1), disabled=bloquear_campo_reg),
                   sg.Button('Alterar', key='bt_alterar', size=(20, 1), disabled=not bloquear_campo_reg),
                   sg.Button('Excluir', button_color='Red', key='bt_excluir', disabled=not bloquear_campo_reg,
                             size=(20, 1))]]

        self.window.Layout(layout).Finalize().GrabAnyWhereOff()

    def mostrar_menu(self, dados=None):
        self.iniciar_nova_janela()
        self.init_components(dados)

        while True:
            try:
                botao, valores = self.window.Read()

                if botao == sg.WIN_CLOSED:
                    break

                if botao in ['bt_alterar', 'bt_novo']:
                    if not valores.get('id'):
                        self.mostrar_mensagem('Erro:', 'insira um id')
                        raise StopIteration

                    if not valores.get('nome'):
                        self.mostrar_mensagem('Erro:', 'insira um nome')
                        raise StopIteration

                self.close()
                return botao, valores
            except StopIteration:
                pass

    def close(self):
        super().close()

    def iniciar_nova_janela(self):
        super().iniciar_nova_janela()

    def mostrar_mensagem(self, nome: str, mensagem: str):
        super().mostrar_mensagem(nome, mensagem)
