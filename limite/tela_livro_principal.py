import sys

from PySimpleGUI import Table

from limite.abstrato.tela_abstrata import TelaAbstrata
import PySimpleGUI as sg


class TelaLivroPrincipal(TelaAbstrata):

    def init_components(self, dados):
        menu_def = [
            ['&Sistema', ['Sair', 'Voltar']],
        ]

        if len(dados) == 0:
            tabela_dados = ['Nenhum livro cadastrado']
        else:
            tabela_dados = dados

        header_list = ['Registro', 'Titulo', 'Autor Nome', 'Categoria', 'Descrição', 'Editora', 'Ativo', 'Disponível',
                       'Observação']
        table = Table(values=tabela_dados,
                      headings=header_list,
                      display_row_numbers=False,
                      auto_size_columns=True,
                      select_mode="browse",
                      num_rows=20,
                      vertical_scroll_only=False,
                      justification='left',
                      key="linha_selecionada")

        layout = [[sg.MenuBar(menu_def)],
                  [sg.Text('Controle dos Livros', justification='center', size=(40, 1))],
                  [table],
                  [sg.Button('Adicionar', key='bt_novo', size=(25, 1)),
                   sg.Button('Alterar', key='bt_alterar', size=(25, 1)),
                   sg.Button('Excluir', button_color='Red', key='bt_excluir', size=(25, 1))]]

        self.window.Layout(layout).Finalize().GrabAnyWhereOff()

    def mostrar_menu(self, dados):
        livro_valores = []
        for livro in dados:
            livro.update({"Autor": livro.get('Autor').get('nome')})
            livro.update({"Editora": livro.get('Editora').get('nome')})
            livro_valores.append(list(livro.values()))

        self.iniciar_nova_janela()
        self.init_components(livro_valores)

        while True:
            try:
                botao, valores = self.window.Read()

                if botao == sg.WIN_CLOSED:
                    sys.exit(0)

                if botao in ['bt_novo', 'Sair', 'Voltar']:
                    self.close()
                    return botao, valores

                linha_selecionada = valores.get('linha_selecionada')[0]

                registro = livro_valores[linha_selecionada][0]

                self.close()
                return botao, registro
            except IndexError:
                self.mostrar_mensagem('Erro:', 'Selecione um livro!')

    def close(self):
        super().close()

    def iniciar_nova_janela(self):
        super().iniciar_nova_janela()

    def mostrar_mensagem(self, titulo: str, mensagem: str):
        super().mostrar_mensagem(titulo, mensagem)
