import sys

from limite.abstrato.tela_abstrata import TelaAbstrata
import PySimpleGUI as sg


class TelaAlunoDetalhe(TelaAbstrata):

    def init_components(self, turmas_dados, dados=None):
        menu_def = [
            ['&Sistema', ['Sair', 'Voltar']],
        ]

        bloquear_campo_reg = True if dados else None

        dados = dados if dados else {}

        tabela_turma = sg.Table (values=[list (turma.values ()) for turma in turmas_dados],
                                 headings=['Id', 'Chave', 'Total Alunos'],
                                 display_row_numbers=False,
                                 auto_size_columns=False,
                                 select_mode="browse",
                                 num_rows=5,
                                 vertical_scroll_only=False,
                                 justification='left',
                                 col_widths=[5, 20, 10],
                                 key="tabela_turma",
                                 background_color='white',
                                 text_color='black')

        frame_layout = [[sg.Text('Matricula:', justification='left', size=(20, 1))],
                        [sg.InputText(disabled=bloquear_campo_reg, default_text=dados.get('Matricula'), key='matricula',
                                      justification='left', size=(10, 1))],

                        [sg.Text('Nome:', justification='left', size=(60, 1))],
                        [sg.InputText(default_text=dados.get('Nome'), size=(50, 1), key='nome')],

                        [sg.Text ('Turma:', justification='left', size=(60, 1))],
                        [tabela_turma]]

        frame_principal = sg.Frame('aluno', layout=frame_layout, element_justification='left')

        layout = [[sg.MenuBar(menu_def)],
                  [sg.Text('Controle dos Alunoes', justification='center', size=(40, 1))],
                  [frame_principal],
                  [sg.Button('Adicionar', key='bt_novo', size=(20, 1), disabled=bloquear_campo_reg),
                   sg.Button('Alterar', key='bt_alterar', size=(20, 1), disabled=not bloquear_campo_reg),
                   sg.Button('Excluir', button_color='Red', key='bt_excluir', disabled=not bloquear_campo_reg,
                             size=(20, 1))]]

        self.window.Layout(layout).Finalize().GrabAnyWhereOff()

    def mostrar_menu(self, turmas_dados, dados=None):
        self.iniciar_nova_janela()
        self.init_components(turmas_dados, dados)

        while True:
            if dados:
                livro_turma_row = None
                turma_count = 0
                for turma in turmas_dados:
                    if turma.get('Id') == dados.get('Turma').get('Id'):
                        livro_turma_row = turma_count
                    turma_count += 1

                self.window.Element ('tabela_turma').Update(select_rows=[livro_turma_row])
                    
            try:
                botao, valores = self.window.Read()

                if botao == sg.WIN_CLOSED:
                    sys.exit(0)

                if botao in ['bt_alterar', 'bt_novo']:
                    if not valores.get('matricula'):
                        self.mostrar_mensagem('Erro:', 'insira um matricula')
                        raise StopIteration

                    if not valores.get('nome'):
                        self.mostrar_mensagem('Erro:', 'insira um título')
                        raise StopIteration

                    try:
                        linha_selecionada_turma = valores.get('tabela_turma')[0]

                        turma = turmas_dados[linha_selecionada_turma]
                        valores.update({"turma": turma})
                    except IndexError:
                        self.mostrar_mensagem('Erro:', 'selecione uma turma')
                        raise StopIteration


                self.close()
                return botao, valores
            except StopIteration:
                pass

    def close(self):
        super().close()

    def iniciar_nova_janela(self):
        super().iniciar_nova_janela()

    def mostrar_mensagem(self, nome: str, mensagem: str):
        super().mostrar_mensagem(nome, mensagem)
