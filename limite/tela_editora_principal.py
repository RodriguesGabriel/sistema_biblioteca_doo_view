from PySimpleGUI import Table

from limite.abstrato.tela_abstrata import TelaAbstrata
import PySimpleGUI as sg


class TelaEditoraPrincipal(TelaAbstrata):

    def init_components(self, dados):
        menu_def = [
            ['&Sistema', ['Sair', 'Voltar']],
        ]

        if len(dados) == 0:
            tabela_dados = ['Nenhuma editora']
        else:
            tabela_dados = dados

        header_list = ['Id', 'Nome']
        table = Table(values=tabela_dados,
                      headings=header_list,
                      display_row_numbers=False,
                      auto_size_columns=False,
                      col_widths=[10, 50],
                      select_mode="browse",
                      num_rows=20,
                      vertical_scroll_only=False,
                      justification='left',
                      key="linha_selecionada")

        layout = [[sg.MenuBar(menu_def)],
                  [sg.Text('Controle dos Editoraes', justification='center', size=(40, 1))],
                  [table],
                  [sg.Button('Adicionar', key='bt_novo', size=(25, 1)),
                   sg.Button('Alterar', key='bt_alterar', size=(25, 1)),
                   sg.Button('Excluir', button_color='Red', key='bt_excluir', size=(25, 1))]]

        self.window.Layout(layout).Finalize().GrabAnyWhereOff()

    def mostrar_menu(self, dados):
        editora_valores = []
        for editora in dados:
            editora_valores.append(list(editora.values()))

        self.iniciar_nova_janela()
        self.init_components(editora_valores)

        while True:
            try:
                botao, valores = self.window.Read()

                if botao in ['bt_novo', 'Sair', 'Voltar']:
                    self.close()
                    return botao, valores

                linha_selecionada = valores.get('linha_selecionada')[0]

                id = editora_valores[linha_selecionada][0]

                self.close()
                return botao, id
            except IndexError:
                self.mostrar_mensagem('Erro:', 'Selecione um editora!')

    def close(self):
        super().close()

    def iniciar_nova_janela(self):
        super().iniciar_nova_janela()

    def mostrar_mensagem(self, titulo: str, mensagem: str):
        super().mostrar_mensagem(titulo, mensagem)
