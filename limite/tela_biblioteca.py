import sys

from limite.abstrato.tela_abstrata import TelaAbstrata
import PySimpleGUI as sg


class TelaBiblioteca(TelaAbstrata):

    def init_components(self):
        menu_def = [
            ['&Sistema', ['Sair']],
        ]

        layout = [[sg.MenuBar(menu_def)],
                  [sg.Text('Sistema Biblioteca', justification='center', size=(20, 1))],
                  [sg.Button('Livro', key='bt_livro', size=(600, 1))],
                  [sg.Button('Aluno', key='bt_aluno', size=(600, 1))],
                  [sg.Button('Turma', key='bt_turma', size=(600, 1))],
                  [sg.Button('Editora', key='bt_editora', size=(600, 1))],
                  [sg.Button('Autor', key='bt_autor', size=(600, 1))],
                  [sg.Button('Emprestimo', key='bt_emprestimo', size=(600, 1))]]

        self.window.Layout(layout).Finalize()

    def mostrar_menu(self):
        self.iniciar_nova_janela()
        self.init_components()
        while True:
            botao, valores = self.window.Read()

            if botao == sg.WIN_CLOSED:
                sys.exit(0)

            self.close()
            return botao, valores

    def close(self):
        super().close()

    def iniciar_nova_janela(self):
        super().iniciar_nova_janela()

    def mostrar_mensagem(self, titulo: str, mensagem: str):
        super().mostrar_mensagem(titulo, mensagem)
