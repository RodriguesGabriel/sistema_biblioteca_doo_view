import sys

from limite.abstrato.tela_abstrata import TelaAbstrata
import PySimpleGUI as sg


class TelaEmprestimoDetalhe(TelaAbstrata):

    def init_components(self, autores_dados, editoras_dados, dados=None):
        menu_def = [
            ['&Sistema', ['Sair', 'Voltar']],
        ]

        bloquear_campo_reg = True if dados else None

        dados = dados if dados else {}

        tabela_autor = sg.Table(values=[list(autor.values()) for autor in autores_dados],
                                headings=list(autores_dados[0].keys()),
                                display_row_numbers=False,
                                auto_size_columns=False,
                                select_mode="browse",
                                num_rows=5,
                                vertical_scroll_only=False,
                                justification='left',
                                col_widths=[5, 20],
                                key="tabela_autor",
                                background_color='white',
                                text_color='black')

        tabela_editora = sg.Table(values=[list(editora.values()) for editora in editoras_dados],
                                  headings=list(editoras_dados[0].keys()),
                                  display_row_numbers=False,
                                  auto_size_columns=False,
                                  select_mode="browse",
                                  num_rows=5,
                                  vertical_scroll_only=False,
                                  justification='left',
                                  col_widths=[5, 20],
                                  key="tabela_editora",
                                  background_color='white',
                                  text_color='black')

        frame_layout = [[sg.Text('Registro:', justification='left', size=(20, 1))],
                        [sg.InputText(disabled=bloquear_campo_reg, default_text=dados.get('Registro'), key='registro',
                                      justification='left', size=(10, 1))],

                        [sg.Text('Titulo:', justification='left', size=(60, 1))],
                        [sg.InputText(default_text=dados.get('Título'), size=(50, 1), key='titulo')],

                        [sg.Text('Autor:', justification='left', size=(40, 1))],
                        [tabela_autor],

                        [sg.Text('Categoria:', justification='left', size=(40, 1))],
                        [sg.InputText(default_text=dados.get('Categoria'), key='categoria', size=(50, 1))],

                        [sg.Text('Editora:', justification='left', size=(40, 1))],
                        [tabela_editora],

                        [sg.Text('Descrição:', justification='left', size=(40, 1))],
                        [sg.InputText(default_text=dados.get('Descricao'), key='descricao', size=(50, 1))],

                        [sg.Text('Ativo:', justification='left', size=(40, 1))],
                        [sg.Checkbox('sim', default=dados.get('Ativo'), key='ativo')],

                        [sg.Text('Observação:', justification='left', size=(40, 1))],
                        [sg.InputText(default_text=dados.get('Observação'), size=(70, 5), key='observacao')]]

        frame_principal = sg.Frame('emprestimo', layout=frame_layout, element_justification='left')

        layout = [[sg.MenuBar(menu_def)],
                  [sg.Text('Controle dos Emprestimos', justification='center', size=(40, 1))],
                  [frame_principal],
                  [sg.Button('Adicionar', key='bt_novo', size=(20, 1), disabled=bloquear_campo_reg),
                   sg.Button('Alterar', key='bt_alterar', size=(20, 1), disabled=not bloquear_campo_reg),
                   sg.Button('Excluir', button_color='Red', key='bt_excluir', disabled=not bloquear_campo_reg,
                             size=(20, 1))]]

        self.window.Layout(layout).Finalize().GrabAnyWhereOff()

    def mostrar_menu(self, autores_dados, editoras_dados, dados=None):
        self.iniciar_nova_janela()
        self.init_components(autores_dados, editoras_dados, dados)

        if dados:
            emprestimo_autor_row = None
            autor_count = 0
            for autor in autores_dados:
                if autor.get('Id') == dados.get('Autor').get('id'):
                    emprestimo_autor_row = autor_count
                autor_count += 1

            emprestimo_editora_row = None
            editora_count = 0
            for editora in editoras_dados:
                if editora.get('Id') == dados.get('Editora').get('id'):
                    emprestimo_editora_row = editora_count
                editora_count += 1

            self.window.Element('tabela_autor').Update(select_rows=[emprestimo_autor_row])
            self.window.Element('tabela_editora').Update(select_rows=[emprestimo_editora_row])

        while True:
            try:
                botao, valores = self.window.Read()

                if botao == sg.WIN_CLOSED:
                    sys.exit(0)

                if botao in ['bt_alterar', 'bt_novo']:
                    try:
                        linha_selecionada_autor = valores.get('tabela_autor')[0]

                        autor = autores_dados[linha_selecionada_autor]
                        valores.update({"autor": autor})
                    except IndexError:
                        self.mostrar_mensagem('Erro:', 'selecione um autor')
                        raise StopIteration

                    try:
                        linha_selecionada_editora = valores.get('tabela_editora')[0]

                        editora = editoras_dados[linha_selecionada_editora]
                        valores.update({"editora": editora})
                    except IndexError:
                        self.mostrar_mensagem('Erro:', 'selecione uma editora')
                        raise StopIteration

                    if not valores.get('registro'):
                        self.mostrar_mensagem('Erro:', 'insira um registro')
                        raise StopIteration

                    if not valores.get('titulo'):
                        self.mostrar_mensagem('Erro:', 'insira um título')
                        raise StopIteration

                    if not valores.get('categoria'):
                        self.mostrar_mensagem('Erro:', 'insira uma categoria')
                        raise StopIteration

                self.close()
                return botao, valores
            except StopIteration:
                pass

    def close(self):
        super().close()

    def iniciar_nova_janela(self):
        super().iniciar_nova_janela()

    def mostrar_mensagem(self, titulo: str, mensagem: str):
        super().mostrar_mensagem(titulo, mensagem)
