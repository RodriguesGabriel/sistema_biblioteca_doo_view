from PySimpleGUI import Table

from limite.abstrato.tela_abstrata import TelaAbstrata
import PySimpleGUI as sg


class TelaTurmaPrincipal(TelaAbstrata):

    def init_components(self, dados):
        menu_def = [
            ['&Sistema', ['Sair', 'Voltar']],
        ]

        if len(dados) == 0:
            tabela_dados = ['Nenhuma turma cadastrada']
        else:
            tabela_dados = dados

        header_list = ['Id', 'Chave', 'Total de Alunos']
        table = Table(values=tabela_dados,
                      headings=header_list,
                      display_row_numbers=False,
                      col_widths=[10, 40, 15],
                      auto_size_columns=False,
                      select_mode="browse",
                      num_rows=20,
                      vertical_scroll_only=False,
                      justification='left',
                      key="linha_selecionada")

        layout = [[sg.MenuBar(menu_def)],
                  [sg.Text('Controle dos Turmas', justification='center', size=(40, 1))],
                  [table],
                  [sg.Button('Adicionar', key='bt_novo', size=(25, 1)),
                   sg.Button('Alterar', key='bt_alterar', size=(25, 1)),
                   sg.Button('Excluir', button_color='Red', key='bt_excluir', size=(25, 1))]]

        self.window.Layout(layout).Finalize().GrabAnyWhereOff()

    def mostrar_menu(self, dados):
        turma_valores = []
        for turma in dados:
            turma_valores.append(list(turma.values()))

        self.iniciar_nova_janela()
        self.init_components(turma_valores)

        while True:
            try:
                botao, valores = self.window.Read()

                if botao in ['bt_novo', 'Sair', 'Voltar']:
                    self.close()
                    return botao, valores

                linha_selecionada = valores.get('linha_selecionada')[0]

                registro = turma_valores[linha_selecionada][0]

                self.close()
                return botao, registro
            except IndexError:
                self.mostrar_mensagem('Erro:', 'Selecione uma turma!')

    def close(self):
        super().close()

    def iniciar_nova_janela(self):
        super().iniciar_nova_janela()

    def mostrar_mensagem(self, titulo: str, mensagem: str):
        super().mostrar_mensagem(titulo, mensagem)
